#!/bin/sh -eux


# Prepare test work directory

WORK_DIR=$(mktemp -d /var/tmp/dirinstall.XXXXXX)


# Create kickstart

KICKSTART_PATH=${WORK_DIR}/ks.cfg
. ./repositories
TEST_KICKSTART=./ks.dirinstall.cfg

echo "==== List of repositories ===="
dnf repoinfo
echo "== End list of repositories =="

if grep -q 'NAME="CentOS Stream"' /etc/os-release ; then
    BASE_REPO="$STREAM_BASE_REPO"
    APP_REPO="$STREAM_APP_REPO"
else
    BASE_REPO="$RHEL_BASE_REPO"
    APP_REPO="$RHEL_APP_REPO"
fi

get_base_url () {
    REPO="$1"

    # This will show repository content and get url from this:
    # Repo-baseurl       : http://ftp.sh.cvut.cz/centos-stream/9-stream/BaseOS/x86_64/os/ (51 more)
    dnf repoinfo "$REPO" | \
        grep ^Repo-baseurl | \
        cut -d: -f2- | \
        sed 's/^ *//' | \
        cut -d' ' -f1
}

# Dump URLs of installation repositories found in local repositories whose names are configured in 'repositories' file
echo "url --url=$(get_base_url $BASE_REPO)" > ${KICKSTART_PATH}
echo "repo --name=$APP_REPO --baseurl=$(get_base_url $APP_REPO)" >> ${KICKSTART_PATH}

cat ${TEST_KICKSTART} >> ${KICKSTART_PATH}

# Log the kickstart
cat ${KICKSTART_PATH}


# Run dirinstall

INSTALL_DIR=${WORK_DIR}/install_dir
mkdir ${INSTALL_DIR}

anaconda --dirinstall ${INSTALL_DIR} --kickstart ${KICKSTART_PATH} --${ANACONDA_UI_MODE} --noninteractive 2>&1


# Remove test work directory

rm -rf ${WORK_DIR}


# Show and remove the logs for this anaconda run

./show_logs.sh
